// pogrebnoy.cpp : Defines the entry point for the console application.
//

#include <iostream>`
#include <vector>
#include <string>
#include <fstream>

using namespace std;
 
int main(int argc, char* argv[])
{
	if (argc != 5)
	{
		cerr << "Error: arguments must be <input_filename.txt> <output_filename.txt> <key> <crypt | encrypt>" << endl;
		return 0;
	}
	else
	{
		string key = argv[3];
		if (key.length()==0)
		{
			cerr << "empty key";
			return 0;
		}
		for (int i = 0; i < key.length(); i++)
		{
			if (!isalpha(key[i]))
			{
				cerr << "Error: key must be a string";
				return 0;
			}
		}
		ifstream ifst(argv[1]);
		ofstream ofst(argv[2]);
		string flag = argv[4];
		string text, cryptedText, encryptedText;
		
		while (!ifst.eof())
		{
			getline(ifst, text);
			cryptedText.resize(text.size());
			int i = 0;
			for (int i = 0; i < text.length(); i++)
			{
				if (flag == "crypt")
				{
					cryptedText[i] = text[i] + key[i%key.length()];
				}
				else
				{
					cryptedText[i] = text[i] - key[i%key.length()];
				}
			}
			ofst << cryptedText << endl;
		}	
	}
 	return 0;
}

