package steganography;

import java.awt.Dimension;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.LinkedList;
import java.util.Queue;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Stego extends JFrame {
	/**
	 * @param args
	 */
	final static int startNumberOfByte = 100;// ���������� ������������ � ������
	// �����
	// ����� ����������� � ����� ���� ���������, ��������� ��� ����� wav
	// �������� 44 �����
	public static void main(String[] args) {
		
		try {
			RandomAccessFile file = new RandomAccessFile("3150.wav", "rw");
			String message = args[0];// ������ ������� ���� �������� � ��� ����
			// �������������
			encryptOurMessage(file, message);
			// �������������
			String decodeMessage = decryptOurMessage(message.length());
			new Stego(decodeMessage);
			System.out.println(decodeMessage);
		} catch (FileNotFoundException e) {
			System.out.print("Caught: " + String.valueOf(e.getMessage()));
		} catch (IOException e) {
			System.out.print("Caught: " + e.getCause());
		}
		

	}

	private static String decryptOurMessage(int messageLen) throws IOException {
		Byte curentByte;
		RandomAccessFile outFileToDecode = new RandomAccessFile(
				"myFileDecode.wav", "rw");
		RandomAccessFile fileToDecode = new RandomAccessFile("myFile.wav", "rw");

		for (int i = 0; i < startNumberOfByte; i++) {
			curentByte = (byte) fileToDecode.read();
			outFileToDecode.writeByte(curentByte);
		}
		String decodeMessage = "";
		for (int i = 0; i < messageLen; i++) {
			byte mask = 0b00000011;// ������� ���� ��������� �������
			curentByte = (byte) fileToDecode.read();
			byte l1, l2, l3, l4;
			l1 = (byte) (curentByte & mask);
			l1 = (byte) (l1 << 6);
			curentByte = (byte) fileToDecode.read();
			l2 = (byte) (curentByte & mask);
			l2 = (byte) (l2 << 4);
			curentByte = (byte) fileToDecode.read();
			l3 = (byte) (curentByte & mask);
			l3 = (byte) (l3 << 2);
			curentByte = (byte) fileToDecode.read();
			l4 = (byte) (curentByte & mask);
			int d = (int) (l1 | l2 | l3 | l4);
			decodeMessage += (char) d;
		}
		return decodeMessage;
	}

	private static void encryptOurMessage(RandomAccessFile file, String message)
			throws IOException {
		Byte curentByte;
		RandomAccessFile outFile = new RandomAccessFile("myFile.wav", "rw");

		Queue<Byte> queueMessage = new LinkedList<Byte>();// ����� �������� ����
															// ��������� �������
		queueMessage = messageToBytes(message);// ��������� ���� ��������� ��
												// ��� ��������� �����

		for (int i = 0; i < startNumberOfByte; i++) {
			curentByte = (byte) file.read();
			outFile.writeByte(curentByte);
		}

		while ((curentByte = (byte) file.read()) != -1) {
			curentByte = doChangeByte(queueMessage, curentByte);
			outFile.writeByte(curentByte);
		}

	}

	private static Queue<Byte> messageToBytes(String message) {
		Queue<Byte> queueMessage = new LinkedList<Byte>();
		for (int i = 0; i < message.length(); i++) {
			queueMessage.add((byte) ((byte) message.charAt(i) >> 6));
			queueMessage.add((byte) (((byte) message.charAt(i) << 2) >> 6));
			queueMessage.add((byte) (((byte) message.charAt(i) << 4) >> 6));
			queueMessage.add((byte) (((byte) message.charAt(i) << 6) >> 6));
		}
		return queueMessage;
	}

	private static Byte doChangeByte(Queue<Byte> queueMessage, Byte b) {
		if (!queueMessage.isEmpty()) {
			b = (byte) (((b >> 2) << 2) + queueMessage.poll());
		}
		return b;
	}

	public Stego(String decodeMessage) {
		JPanel panel1 = new JPanel();
		JLabel label1 = new JLabel(decodeMessage);
		panel1.add(label1);
		JButton convert;
		convert = new JButton("convert");
		convert.setSize(100, 40);
		//convert.setSize(new Dimension(20+WIDTH/4, 30+HEIGHT/4));
			
		convert.setVisible(true);
		this.add(convert);
		this.add(panel1);
		this.setTitle("Stganography");
		this.setSize(500, 500);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
	}
}
