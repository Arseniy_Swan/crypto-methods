package com.example.user.steganography;

import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.LinkedList;
import java.util.Queue;


public class MainActivity extends ActionBarActivity {
    final static String DIR_SD = "MyFiles";
    final static int startNumberOfByte = 100;// переменная определяющая с какого
    /*
    байта
    будет зашифровано в файле наше сообщение, заголовок для файла wav
    занимает 44 байта
    */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnStego = (Button) findViewById(R.id.btnStego);
        Button btnDecode = (Button) findViewById(R.id.btnDecode);

        final EditText edInputMessage = (EditText) findViewById(R.id.edInputMessage);
        final EditText edDestinationFile = (EditText) findViewById(R.id.edDestenationFileStego);
        final EditText edOutputFile  = (EditText) findViewById(R.id.edOutputFile);
        final TextView tvOutputMessage = (TextView) findViewById(R.id.tvOutputMessage);
        final EditText edSourceFile = (EditText) findViewById(R.id.edSourceFileStego);

        View.OnClickListener stegoMessage = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String nameFile = String.valueOf(edSourceFile.getText());
                String message = String.valueOf(edInputMessage.getText());
                message+='#';
                String nameOutFile = String.valueOf(edDestinationFile.getText());

                // строка которую надо вставить в наш файл
                // зашифровываем
                try {
                    encryptOurMessage(nameFile, message, nameOutFile);
                } catch (IOException e) {
                    e.printStackTrace();
                    tvOutputMessage.setText("Bad input file");
                }
            }
        };
        btnStego.setOnClickListener(stegoMessage);

        // декодирование
        View.OnClickListener decodeMessage = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int len = edInputMessage.getText().length();
                String nameFile = String.valueOf(edOutputFile.getText());
                String decodeMessage = null;

                try {
                    decodeMessage = decryptOurMessage(len, nameFile);
                    tvOutputMessage.setText(decodeMessage);
                } catch (IOException e) {
                    e.printStackTrace();
                    tvOutputMessage.setText("Bad output file");
                }
            }
        };
        btnDecode.setOnClickListener(decodeMessage);

    }


    private static String decryptOurMessage(int messageLen, String nameFile) throws IOException {
        Byte currentByte;
        if (!Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
           // Log.d(LOG_TAG, "SD-карта не доступна: " + Environment.getExternalStorageState());
            return null;
        }
        // получаем путь к SD
        File sdPath = Environment.getExternalStorageDirectory();
        // добавляем свой каталог к пути
        sdPath = new File(sdPath.getAbsolutePath() + "/" + DIR_SD);
        // формируем объект File, который содержит путь к файлу
        File sdFile = new File(sdPath, nameFile);

        RandomAccessFile fileToDecode = new RandomAccessFile(sdFile, "rw");

        for (int i = 0; i < startNumberOfByte; i++) {
            currentByte = (byte) fileToDecode.read();
        }

        String decodeMessage = "";
        int d=0;
        while ((char)d!='#'){
            byte mask = 0b00000011;// хватаем наши последние кусочки
            currentByte = (byte) fileToDecode.read();
            byte l1, l2, l3, l4;
            l1 = (byte) (currentByte & mask);
            l1 = (byte) (l1 << 6);
            currentByte = (byte) fileToDecode.read();
            l2 = (byte) (currentByte & mask);
            l2 = (byte) (l2 << 4);
            currentByte = (byte) fileToDecode.read();
            l3 = (byte) (currentByte & mask);
            l3 = (byte) (l3 << 2);
            currentByte = (byte) fileToDecode.read();
            l4 = (byte) (currentByte & mask);
            d = (int) (l1 | l2 | l3 | l4);
            decodeMessage += (char) d;
        }
        decodeMessage = decodeMessage.substring(0,decodeMessage.length()-1);
        return decodeMessage;
    }

    private static void encryptOurMessage(String nameFile, String message, String nameOutFile)
            throws IOException {
        if (!Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            Log.e(LOCATION_SERVICE, "bad card");
            // Log.d(LOG_TAG, "SD-карта не доступна: " + Environment.getExternalStorageState());
            return;
        }
        // получаем путь к SD
        File sdPath = Environment.getExternalStorageDirectory();
        // добавляем свой каталог к пути
        sdPath = new File(sdPath.getAbsolutePath() + "/" + DIR_SD);
        // создаем каталог
        //sdPath.mkdirs();
        // формируем объект File, который содержит путь к файлу
        File sdInputFile = new File(sdPath, nameFile);
        File sdOutFile = new File(sdPath, nameOutFile);

        RandomAccessFile file = new RandomAccessFile(sdInputFile, "rw");
        RandomAccessFile outFile = new RandomAccessFile(sdOutFile, "rw");
        Byte currentByte;
        Queue<Byte> queueMessage = new LinkedList<Byte>();// здесь хранятся наши
        // двубитные кусочки
        queueMessage = messageToBytes(message);// разбиваем наше сообщение на
        // эти двубитные куски

        for (int i = 0; i < startNumberOfByte; i++) {
            currentByte = (byte) file.read();
            outFile.writeByte(currentByte);
        }

        while ((currentByte = (byte) file.read()) != -1) {
            currentByte = doChangeByte(queueMessage, currentByte);
            outFile.writeByte(currentByte);
        }
    }

    private static Queue<Byte> messageToBytes(String message) {
        Queue<Byte> queueMessage = new LinkedList<Byte>();
        for (int i = 0; i < message.length(); i++) {
            queueMessage.add((byte) ((byte) message.charAt(i) >> 6));
            queueMessage.add((byte) (((byte) message.charAt(i) << 2) >> 6));
            queueMessage.add((byte) (((byte) message.charAt(i) << 4) >> 6));
            queueMessage.add((byte) (((byte) message.charAt(i) << 6) >> 6));
        }
        return queueMessage;
    }

    private static Byte doChangeByte(Queue<Byte> queueMessage, Byte b) {
        if (!queueMessage.isEmpty()) {
            b = (byte) (((b >> 2) << 2) + queueMessage.poll());
        }
        return b;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
