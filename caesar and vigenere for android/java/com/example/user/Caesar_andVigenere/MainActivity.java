package com.example.user.Caesar_andVigenere;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

public class MainActivity extends ActionBarActivity {
    final static String DIR_SD = "MyFiles";
    final static int startNumberOfByte = 100;// переменная определяющая с какого
    private static final String LOGTAG = "MainActivity";
    private final int REQUEST_CODE_PICK_DIR = 1;
    private final int REQUEST_CODE_PICK_FILE = 2;
    public static String sourcePath = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final  Activity activityForButton = this;

        final Button btnCryptCaesar, btnCrypVigenere, btnChoseFileEncode;
        final TextView tvCaesar, tvVigener;
        final EditText etCaesar, etVigener, etCaesarKey, etVigenerKey;


        etCaesarKey = (EditText) findViewById(R.id.caesarKeyEditText);
        etCaesar = (EditText) findViewById(R.id.caesarEditText);

        etVigener = (EditText) findViewById(R.id.vigenerEditText);
        etVigenerKey = (EditText) findViewById(R.id.vigenereKeyEditText);

        tvVigener = (TextView) findViewById(R.id.vigenereCrypt);
        tvCaesar = (TextView) findViewById(R.id.caesarCrypt);

        btnCryptCaesar = (Button) findViewById(R.id.btnCaesarCrypt);
        btnCrypVigenere = (Button) findViewById(R.id.btnVigenereCrypt);

        btnChoseFileEncode = (Button) findViewById(R.id.btnChoseFileEncode);

        final CheckBox chBoxCaesar = (CheckBox) findViewById(R.id.checkBoxCaesar);
        final CheckBox chBoxVigenere = (CheckBox) findViewById(R.id.checkBoxVigenere);

        View.OnClickListener chBoxListener;
        chBoxListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(chBoxCaesar.isChecked())
                {
                    btnCryptCaesar.setText("decrypt");
                }
                else btnCryptCaesar.setText("crypt");
            }
        };
        chBoxCaesar.setOnClickListener(chBoxListener);
        View.OnClickListener chBoxListenerVigenere;
        chBoxListenerVigenere = new OnClickListener(){

            @Override
            public void onClick(View v) {
                if(chBoxVigenere.isChecked())
                {
                    btnCrypVigenere.setText("decrypt");
                }
                else btnCrypVigenere.setText("crypt");
            }
        };
        chBoxVigenere.setOnClickListener(chBoxListenerVigenere);

        View.OnClickListener search = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent fileExploreIntent = new Intent(
                        FileBrowserActivity.INTENT_ACTION_SELECT_FILE,
                        null,
                        activityForButton,
                        FileBrowserActivity.class
                );

                startActivityForResult(
                        fileExploreIntent,
                        REQUEST_CODE_PICK_FILE
                /*
                //для папки
                Log.d(LOGTAG, "Start browsing button pressed");
                Intent fileExploreIntent = new Intent(
                        FileBrowserActivity.INTENT_ACTION_SELECT_DIR,null,activityForButton,FileBrowserActivity.class
                );
                startActivityForResult(
                        fileExploreIntent,
                        REQUEST_CODE_PICK_DIR*/
                );
            }
        };
        btnChoseFileEncode.setOnClickListener(search);

       OnClickListener onClickBtnCaesar = new OnClickListener() {
            public void onClick(View v) {
                // Меняем текст в TextView (tvCaesar)
                try {
                    if (ExecutedModule.cryptCaesar(etCaesar, etCaesarKey,chBoxCaesar) != "false") {
                        tvCaesar.setText(String.valueOf(ExecutedModule.cryptCaesar(etCaesar, etCaesarKey, chBoxCaesar)));
                    }
                    else {
                        tvCaesar.setText("ERROR: key must be smaller than 26, or Bad file format. must be .txt");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        btnCryptCaesar.setOnClickListener(onClickBtnCaesar);
        OnClickListener onClickBtnVigenere = new OnClickListener() {
            public void onClick(View v) {
                try {
                    if(ExecutedModule.cryptVigenere(etVigener, etVigenerKey,chBoxVigenere) != "false")
                    {
                        tvVigener.setText(String.valueOf((ExecutedModule.cryptVigenere(etVigener,etVigenerKey,chBoxVigenere))));
                    }
                    else {
                        tvVigener.setText("ERROR: key must be different, or Bad file format. must be .txt");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
         };
        btnCrypVigenere.setOnClickListener(onClickBtnVigenere);
        //Button btnCryptVigenere = (Button) findViewById(R.id.btnVigenereCrypt);
        //btnCryptVigenere.setOnClickListener(onClickBtnCaesar);
       // btnOk = (Button) findViewById(R.id.btnOk);
    }

    @Override
    public View findViewById(int id) {
        return super.findViewById(id);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }


        return super.onOptionsItemSelected(item);
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PICK_DIR) {
            if(resultCode == RESULT_OK) {
                String newDir = data.getStringExtra(//получаем строку с папкой
                        FileBrowserActivity.returnDirectoryParameter);

                Toast.makeText(
                        this,
                        "Received DIRECTORY path from file browser:\n" + newDir,
                        Toast.LENGTH_LONG).show();

            } else {//if(resultCode == this.RESULT_OK) {
                Toast.makeText(
                        this,
                        "Received NO result from file browser",
                        Toast.LENGTH_LONG).show();
            }//END } else {//if(resultCode == this.RESULT_OK) {
        }//if (requestCode == REQUEST_CODE_PICK_DIR) {

        if (requestCode == REQUEST_CODE_PICK_FILE) {
            if(resultCode == RESULT_OK) {
                String newFile = data.getStringExtra(
                        FileBrowserActivity.returnFileParameter);
                sourcePath = newFile;
                EditText edCaesar = (EditText) findViewById(R.id.caesarEditText);
                EditText edVigenere = (EditText) findViewById(R.id.vigenerEditText);
                edCaesar.setText(newFile);
                edVigenere.setText(newFile);
                //edSourceFile.setText(newFile);
                Toast.makeText(
                        this,
                        "Received FILE path from file browser:\n"+newFile,
                        Toast.LENGTH_LONG).show();

            } else {//if(resultCode == this.RESULT_OK) {
                Toast.makeText(
                        this,
                        "Received NO result from file browser",
                        Toast.LENGTH_LONG).show();
            }//END } else {//if(resultCode == this.RESULT_OK) {
        }//if (requestCode == REQUEST_CODE_PICK_FILE) {


        super.onActivityResult(requestCode, resultCode, data);

    }
}
