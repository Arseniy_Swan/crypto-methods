package com.example.user.Caesar_andVigenere;

import android.os.Environment;
import android.util.Log;
import android.widget.CheckBox;
import android.widget.EditText;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;

import static java.lang.String.valueOf;

/**
 * Created by user on 15.04.2015.
 */
public class ExecutedModule extends MainActivity {

    static String cryptCaesar(EditText etCaesar, EditText etCaesarKey,CheckBox chBoxCaesar)
            throws IOException {
        if (!Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            Log.e(LOCATION_SERVICE, "bad card");
            // Log.d(LOG_TAG, "SD-карта не доступна: " + Environment.getExternalStorageState());
            return "false";
        }
        boolean itIsEncrypt = !chBoxCaesar.isChecked();
        int key = Integer.valueOf(valueOf(etCaesarKey.getText()));
        String nameFile = valueOf(etCaesar.getText());
        if (nameFile == null || nameFile.equals("")  || !nameFile.contains(".txt")) {
            return "false";
        }
        // получаем путь к SD
        File sdPath = Environment.getExternalStorageDirectory();
        int lenOfNameFile = 1;
        while (nameFile.charAt(nameFile.length() - lenOfNameFile - 1) != '/') {
            lenOfNameFile++;
        }
        sdPath = new File(nameFile.substring(0, nameFile.length() - lenOfNameFile));
        nameFile = nameFile.substring(nameFile.length() - lenOfNameFile, nameFile.length());

        // формируем объект File, который содержит путь к файлу
        File sdInputFile = new File(sdPath, nameFile);
        String nameOutFile;
        if (itIsEncrypt){
            nameOutFile = "encryptCaesar_" + nameFile;
        }
        else nameOutFile = "decryptCaesar_" + nameFile;
        File sdOutFile = new File(sdPath, nameOutFile);
        //RandomAccessFile file = new RandomAccessFile(sdInputFile, "rw");
        BufferedReader file = new BufferedReader(new FileReader(sdInputFile) );
        BufferedWriter outFile = new BufferedWriter(new FileWriter(sdOutFile));
        //RandomAccessFile outFile = new RandomAccessFile(sdOutFile, "rw");
        String buf,text = "";

        while ((buf = file.readLine())!=null)
        {
            text +=buf;
        }
        file.close();
        if (key > 26) {
            return valueOf(false);
        }
        int signEncryptDecrypt;
        if (itIsEncrypt) {
            signEncryptDecrypt = 1;
        }
        else signEncryptDecrypt = -1;
        String encryptedText = "";
        text = text.toLowerCase();
        for (int i = 0; i < text.length(); i++) {
            if(text.charAt(i) == ' ')
                encryptedText+=' ';
            else {
                encryptedText += String.valueOf((char) (((text.charAt(i) % 'a' + key * signEncryptDecrypt) +26)% 26 + 'a'));
            }
        }
        outFile.write(encryptedText);
        outFile.close();
        return encryptedText;
    }

    static String cryptVigenere(EditText etVigenere, EditText etVigenereKey,CheckBox chBoxViigenere)
        throws IOException {
        if (!Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            Log.e(LOCATION_SERVICE, "bad card");
            // Log.d(LOG_TAG, "SD-карта не доступна: " + Environment.getExternalStorageState());
            return "false";
        }
        boolean itIsEncrypt = !chBoxViigenere.isChecked();
        String nameFile = valueOf(etVigenere.getText());
        if (nameFile == null || nameFile.equals("")|| !nameFile.contains(".txt")) {
            return "false";
        }
        // получаем путь к SD
        File sdPath = Environment.getExternalStorageDirectory();
        int lenOfNameFile = 1;
        while (nameFile.charAt(nameFile.length() - lenOfNameFile - 1) != '/') {
            lenOfNameFile++;
        }
        sdPath = new File(nameFile.substring(0, nameFile.length() - lenOfNameFile));
        nameFile = nameFile.substring(nameFile.length() - lenOfNameFile, nameFile.length());

        // формируем объект File, который содержит путь к файлу
        File sdInputFile = new File(sdPath, nameFile);
        String nameOutFile;
        if (itIsEncrypt){
            nameOutFile = "encryptVigenere_" + nameFile;
        }
        else nameOutFile = "decryptVigenere_" + nameFile;
        File sdOutFile = new File(sdPath, nameOutFile);
        BufferedReader file = new BufferedReader(new FileReader(sdInputFile) );
        BufferedWriter outFile = new BufferedWriter(new FileWriter(sdOutFile));

        //RandomAccessFile file = new RandomAccessFile(sdInputFile, "rw");
        //RandomAccessFile outFile = new RandomAccessFile(sdOutFile, "rw");
        String buf,text = "";

        while ((buf = file.readLine())!=null)
        {
            text +=buf;
        }
        file.close();
        String key = String.valueOf(etVigenereKey.getText());
        key = key.toLowerCase();
        //text = String.valueOf(etVigenere.getText());
        text = text.toLowerCase();
        String encryptedText = "";

        int signEncryptDecrypt;
        if (itIsEncrypt) {
            signEncryptDecrypt = 1;
        }
        else signEncryptDecrypt = -1;
        for (int i = 0; i < text.length(); i++) {
            if(text.charAt(i) == ' ')
                encryptedText+=' ';
            else
                encryptedText += String.valueOf((char) (((text.charAt(i) % 'a' + (key.charAt(i%key.length())-'a')*signEncryptDecrypt)+26) % 26 + 'a'));
        }
        outFile.write(encryptedText);
        outFile.close();
        return encryptedText;
    }

}

