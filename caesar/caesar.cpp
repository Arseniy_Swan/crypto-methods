#include <iostream>`
#include <vector>
#include <string>
#include <fstream>

using namespace std;

int main(int argc, char* argv[])
{
	if (argc != 5)
	{
		cerr << "Error: arguments must be <input_filename.txt> <output_filename.txt> <key> <crypt | encrypt>" << endl;
		return 0;
	}
	else
	{
		int key = 0;
		string strKey = argv[3];
		if (strKey.length() == 0)
		{
			cerr << "empty key";
			return 0;
		}
		for (int i = 0; i < strKey.length(); i++)
		{
			if (!isdigit(strKey[i]))
			{
				cerr << "Error: key must be a number";
				return 0;
			}
		}
		key = atoi(argv[3]);
		if (key>26)
		{
			cerr << "key must be smaller than 100";
			return 0;
		}
		ifstream ifst(argv[1]);
		ofstream ofst(argv[2]);
		if (ifst.fail() || ofst.fail())
		{
			cerr << "problem with input/output files";
			return 0;
		}
		string flag = argv[4];
		string text, cryptedText, encryptedText;

		while (!ifst.eof())
		{
			getline(ifst, text);
			cryptedText.resize(text.size());
			int i = 0;
			for (int i = 0; i < text.length(); i++)
			{
				if (flag == "crypt")
				{
					cryptedText[i] = (tolower(text[i]) % 97 + key) % 26 + 97;
				}
				else
				{
					cryptedText[i] = (tolower(text[i]) % 97 - key + 26) % 26 + 97;;
				}
			}
			ofst << cryptedText << endl;
		}
	}
	return 0;
}

